import { LoanStatus } from '@/schema';

export interface Transition {
  targetStatus: LoanStatus;
  label: string;
  icon: string;
}

const statusTransitions: Record<LoanStatus, Transition[]> = {
  submitted: [
    {
      targetStatus: 'approved',
      label: 'Approve',
      icon: 'fas fa-handshake'
    },
    {
      targetStatus: 'cancelled',
      label: 'Cancel',
      icon: 'fas fa-times'
    }
  ],
  approved: [
    {
      targetStatus: 'collected',
      label: 'Collect',
      icon: 'fas fa-truck-loading'
    },
    {
      targetStatus: 'cancelled',
      label: 'Cancel',
      icon: 'fas fa-times'
    }
  ],
  collected: [
    {
      targetStatus: 'returned',
      label: 'Return',
      icon: 'fas fa-truck-moving'
    }
  ],
  returned: [],
  cancelled: []
};

export const getNextStatusCount = (currentStatus: LoanStatus): number => statusTransitions[currentStatus].length;

export const getNextStatus = (currentStatus: LoanStatus, position: 0 | 1 = 0): Transition | undefined =>
  statusTransitions[currentStatus].length <= position ? undefined : statusTransitions[currentStatus][position];

export const isValidTransition = (currentStatus: LoanStatus, nextStatus: LoanStatus): boolean =>
  statusTransitions[currentStatus].map(({ targetStatus }) => targetStatus).includes(nextStatus);
