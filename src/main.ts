import Vue, { CreateElement } from 'vue';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import VueMeta from 'vue-meta';
import { config } from 'dotenv';
import { initialiseAuthenticationService } from '@/services/auth';

config();

Vue.config.productionTip = false;

Vue.use(VueMeta, {
  refreshOnceOnNavigation: true
});

initialiseAuthenticationService()
  .then(() => {
    const render = (h: CreateElement) => h(App);
    new Vue({ router, vuetify, render }).$mount('#app');
  });
