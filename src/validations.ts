export const required = (message: string) => (v: string) => (v || '').length > 0 || message || 'This field is required';
