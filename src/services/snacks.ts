import Vue from 'vue';

export interface Snack {
  message: string;
  type: 'success' | 'error' | undefined;
  visible: boolean;
}

export const snack = Vue.observable<Snack>({
  message: '',
  type: undefined,
  visible: false
});

export const showSnack = (message: string, type?: 'success' | 'error' | undefined) => {
  snack.message = message;
  snack.type = type;
  snack.visible = true;
};

export const hideSnack = () => {
  snack.visible = false;
};
