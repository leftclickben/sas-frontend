import axios from 'axios';
import { CreateLoanPayload, ID, Loan, LoanStatus } from '@/schema';
import { authenticateApiRequest } from '@/services/auth';

export const submitLoanRequest = async (request: CreateLoanPayload): Promise<void> => {
  await axios.post(
    `${process.env.VUE_APP_API_BASE_URL}/loans`,
    request,
    authenticateApiRequest());
};

export const listLoansByStatus = async (status: LoanStatus): Promise<Loan[]> => {
  const response = await axios.get<Loan[]>(
    `${process.env.VUE_APP_API_BASE_URL}/loans/status/${status}`,
    authenticateApiRequest());
  return response.data;
};

export const getLoanById = async (id: ID): Promise<Loan> => {
  const response = await axios.get<Loan>(
    `${process.env.VUE_APP_API_BASE_URL}/loans/id/${id}`,
    authenticateApiRequest());
  return response.data;
};

export const transitionLoan = async (id: ID, status: LoanStatus, comment?: string): Promise<void> => {
  await axios.patch(
    `${process.env.VUE_APP_API_BASE_URL}/loans/id/${id}`,
    { status, comment },
    authenticateApiRequest());
};
