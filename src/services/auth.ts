import Vue from 'vue';
import { AuthenticationDetails, CognitoUser, CognitoUserPool, CognitoUserSession } from 'amazon-cognito-identity-js';
import { AxiosRequestConfig } from 'axios';

export interface AuthState {
  username?: string;
  groups?: string[];
  identityToken?: string;
}

export const authState = Vue.observable<AuthState>({
  username: undefined,
  groups: undefined,
  identityToken: undefined
});

const getCognitoUserPool = (() => {
  let pool: CognitoUserPool | undefined;

  return () => {
    if (!pool) {
      pool = new CognitoUserPool({
        UserPoolId: process.env.VUE_APP_AUTH_USER_POOL_ID as string,
        ClientId: process.env.VUE_APP_AUTH_CLIENT_ID as string
      });
    }
    return pool;
  };
})();

const getCognitoUser: (username: string) => CognitoUser = (() => {
  // Store the created users in a map so we can persist a session between `login` and `completeNewPasswordChallenge`.
  const createdCognitoUsers: Record<string, CognitoUser> = {};

  return (username: string) => {
    if (createdCognitoUsers[username]) {
      return createdCognitoUsers[username];
    }
    const user = new CognitoUser({
      Username: username,
      Pool: getCognitoUserPool()
    });
    createdCognitoUsers[username] = user;
    return user;
  };
})();

const markLoggedIn = (username: string, session: CognitoUserSession) => {
  authState.username = username;
  authState.groups = session.getIdToken().decodePayload()['cognito:groups'];
  authState.identityToken = session.getIdToken().getJwtToken();
};

const markLoggedOut = () => {
  authState.username = undefined;
  authState.groups = undefined;
  authState.identityToken = undefined;
};

export const initialiseAuthenticationService = async (): Promise<void> =>
  new Promise((resolve, reject) => {
    if (!process.env.VUE_APP_AUTH_USER_POOL_ID || !process.env.VUE_APP_AUTH_CLIENT_ID) {
      console.warn('Environment variables missing, authentication disabled');
      return resolve();
    }

    const user = getCognitoUserPool().getCurrentUser();
    if (!user) {
      markLoggedOut();
      return resolve();
    }
    user.getSession(async (sessionError: any, session: CognitoUserSession) => {
      if (sessionError) {
        markLoggedOut();
        return reject(sessionError);
      }
      if (!session.isValid()) {
        console.warn(`Invalid session found for "${user.getUsername()}`);
        markLoggedOut();
        return reject('User session is invalid');
      }
      user.getUserAttributes((attributesError, attributes) => {
        if (attributesError) {
          return reject(attributesError);
        }
        const emailAttribute = (attributes || []).find((attribute) => attribute.getName() === 'email');
        if (!emailAttribute) {
          return reject(`Could not find email address attribute for user ${user.getUsername()}`);
        }
        markLoggedIn(emailAttribute.getValue(), session);
        resolve();
      });
    });
  });

export type AuthCondition = 'unauthenticated' | 'logged-in' | 'new-password-required';

export interface AuthResult {
  condition: AuthCondition;
  message?: string;
}

export const login = async (username: string, password: string): Promise<AuthResult> =>
  new Promise((resolve, reject) =>
    getCognitoUser(username)
      .authenticateUser(
        new AuthenticationDetails({ Username: username, Password: password }),
        {
          onSuccess: async (session: any) => {
            if (!session.isValid()) {
              console.warn(`Invalid session found for "${username}`);
              markLoggedOut();
              return reject('User session is invalid');
            }
            console.info(`Login successful for "${username}"`);
            markLoggedIn(username, session);
            resolve({
              condition: 'logged-in'
            });
          },
          newPasswordRequired: () => {
            console.info(`New password required for initial login of "${username}"`);
            resolve({
              condition: 'new-password-required',
              message: 'You are required to change your password.  Please enter a new password twice below.'
            });
          },
          onFailure: (error: any) => {
            console.error(`Login for "${username}" failed due to error`, error);
            markLoggedOut();
            reject(error);
          }
        }));

export const completeNewPasswordChallenge =
  async (username: string, newPassword: string): Promise<void> =>
    new Promise((resolve, reject) =>
      getCognitoUser(username)
        .completeNewPasswordChallenge(
          newPassword,
          {},
          {
            onSuccess: async (session: any) => {
              if (!session.isValid()) {
                console.warn(`Invalid session found for "${username}`);
                markLoggedOut();
                return reject('User session is invalid');
              }
              console.info(`New password challenge completed successfully for "${username}"`);
              markLoggedIn(username, session);
              resolve();
            },
            onFailure: (error: any) => {
              console.error(`Password challenge failed for "${username}" due to error`, error);
              markLoggedOut();
              reject(error);
            }
          }));

export const logout = async (): Promise<void> => {
  if (!authState.username) {
    return;
  }
  getCognitoUser(authState.username as string).signOut();
  markLoggedOut();
  console.info('Logged out');
};

export const forgotPassword = async (username: string): Promise<AuthResult> =>
  new Promise((resolve, reject) =>
    getCognitoUser(username).forgotPassword({
      onSuccess: ({ CodeDeliveryDetails: { DeliveryMedium: medium, Destination: destination } }: any) => {
        console.info(`User "${username}" has been requested to input verification code`);
        resolve({
          condition: 'new-password-required',
          message: `A verification code was sent by ${medium.toLowerCase()} to ${destination}. ` +
            'Please enter the verification code and set a new password here.'
        });
      },
      onFailure: (error: any) => {
        console.error(`Forgot password failed for "${username}" due to error`, error);
        reject(error);
      }
    }));

export const confirmPassword =
  async (username: string, password: string, verificationCode: string): Promise<AuthResult> =>
    new Promise((resolve, reject) =>
      getCognitoUser(username).confirmPassword(verificationCode, password, {
        onSuccess: () => {
          resolve({
            condition: 'unauthenticated',
            message: 'Your password has been reset, please login now.'
          });
        },
        onFailure: (error: any) => {
          console.error(`Password confirmation failed for "${username}" due to error`, error);
          reject(error);
        }
      }));

export const minimumPasswordLength = 15;

export const isValidPassword = (password: string) => password.length >= minimumPasswordLength;

export const authenticateApiRequest = (config: AxiosRequestConfig = {}) => {
  if (authState.identityToken) {
    config.headers = {
      Authorization: authState.identityToken
    };
  }
  return config;
};
