import axios from 'axios';
import { Item } from '@/schema';
import { authenticateApiRequest } from '@/services/auth';

export const listItems = async (): Promise<Item[]> => {
  const response = await axios.get<Item[]>(
    `${process.env.VUE_APP_API_BASE_URL}/items`,
    authenticateApiRequest());
  return response.data;
};

export const getItemById = async (id: string): Promise<Item> => {
  const response = await axios.get<Item>(
    `${process.env.VUE_APP_API_BASE_URL}/items/id/${id}`,
    authenticateApiRequest());
  return response.data;
};
