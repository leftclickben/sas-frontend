export const formatThousands = (amount: number): string => {
  const length = String(amount).length;
  return String(amount)
    .split('')
    .reduce((result, digit, index) => `${result}${index > 0 && (length - index) % 3 === 0 ? ',' : ''}${digit}`, '');
};

export const formatTimestamp = (timestamp: number): string => new Date(timestamp).toISOString().split('T')[0];

export const formatTitle = (input: string): string =>
  input
    .split(/[\s-]+/)
    .map((word) => `${word.substring(0, 1).toUpperCase()}${word.substring(1)}`)
    .join(' ');
