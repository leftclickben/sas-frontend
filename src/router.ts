import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import PageNotFound from '@/views/PageNotFound.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/help',
      name: 'help',
      component: () => import(/* webpackChunkName: "help" */ './views/Help.vue')
    },
    {
      path: '/item/:id',
      name: 'item',
      component: () => import(/* webpackChunkName: "items" */ './views/ItemDetails.vue')
    },
    {
      path: '/request-loan',
      name: 'requestLoan',
      component: () => import(/* webpackChunkName: "loans" */ './views/LoanRequestForm.vue')
    },
    {
      path: '/loans/by-status/:status',
      name: 'loansByStatus',
      component: () => import(/* webpackChunkName: "loans" */ './views/LoansByStatus.vue')
    },
    {
      path: '/loans/:id',
      name: 'loan',
      component: () => import(/* webpackChunkName: "loans" */ './views/LoanDetails.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "auth" */ './views/Login.vue')
    },
    {
      path: '*',
      name: 'page-not-found',
      component: PageNotFound
    }
  ],
});
