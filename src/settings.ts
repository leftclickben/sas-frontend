export interface Settings {
  title: string;
  tagline: string;
  displayDateFormat: string;
  displayTimeFormat: string;
}

export const getSettings = (): Settings => ({
  title: 'Squad Activity Service',
  tagline: 'Requisition supplies for your next training exercise.',
  displayDateFormat: 'YYYY-MM-DD',
  displayTimeFormat: 'HH:mm'
});
