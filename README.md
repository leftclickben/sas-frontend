# sas-frontend

## Project setup

1. Install dependencies:
    ```
    npm install
    ```
2. Configure a `.env` file by copying `.template.env` and filling in the values from your API deployment
3. Make any desired changes to `src/settings.ts`

## Scripts

* Compiles and hot-reloads for development
    ```
    npm run serve
    ```
* Compiles and minifies for production
    ```
    npm run build
    ```
* Run your tests
    ```
    npm run test
    ```
* Lints and fixes files
    ```
    npm run lint
    ```
